﻿using System;
using dotnetcore.Selenium;
using NUnit.Framework;
using OpenQA.Selenium;

namespace dotnetcore.Tests
{
    [TestFixture]
    public class Test
    {

        [SetUp]
        public void BeforeEach()
        {
            Driver.Init();
            //Pages.Pages.Init();
            Driver.GoTo("https://symphony.is");

        }
        [TearDown]
        public void AfterEach()
        {
            Driver.Current.Quit();
        }

        [Test]
        public void TestTest()
        {
            //
            Assert.IsTrue(Driver.FindElement(By.XPath("//span[contains(text(),'About Us')]")).Displayed);
        }

    }
}

