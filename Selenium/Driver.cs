﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;

namespace dotnetcore.Selenium
{
    public static class Driver
    {
        [ThreadStatic]
        public static IWebDriver _driver;
        [ThreadStatic]
        public static Wait Wait;
        public static void Init()
        {
            //_driver = new ChromeDriver();
            _driver = new ChromeDriver(Path.GetFullPath(@"../../../../" + "Drivers"));
            _driver.Manage().Window.Maximize();
            Wait = new Wait(5);
        }
        public static IWebDriver Current => _driver ?? throw new NullReferenceException("Driver is null.");

        public static void GoTo(string url)
        {
            if (!url.StartsWith("http"))
            {
                url = $"http://{url}";
            }
            Current.Navigate().GoToUrl(url);
        }
        public static IWebElement FindElement(By by)
        {
            return Current.FindElement(by);
        }
        public static IList<IWebElement> FindElements(By by)
        {
            return Current.FindElements(by);
        }
    }
}

